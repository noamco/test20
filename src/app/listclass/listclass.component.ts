import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-listclass',
  templateUrl: './listclass.component.html',
  styleUrls: ['./listclass.component.css']
})
export class ListclassComponent implements OnInit {

  classifeds$:Observable<any[]>;
  users$:Observable<any[]>;
  userId:string;
  
  constructor(private ClassifyService:ClassifyService,
    public authService:AuthService) { }

  ngOnInit() {
    this.users$ = this.authService.getusers()
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.classifeds$ = this.ClassifyService.getlist(this.userId); 
      }
    )
  }

  
  
  }



