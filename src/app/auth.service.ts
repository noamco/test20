import { Router } from '@angular/router';
import { User } from './interfaces/user';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<User | null>;
  
  constructor(public afAuth:AngularFireAuth,
              private router:Router,
              private db: AngularFirestore,) {
    this.user = this.afAuth.authState;
  }

  getUser(){
    return this.user
  }

  SignUp(email:string, password:string){
    this.afAuth
        .auth
        .createUserWithEmailAndPassword(email,password)
        .catch((error)=>alert(error.message)).then(user => {
      
          if (user){this.router.navigate (['/welcome'])}
          else {this.router.navigate (['/login'])
          // this.errorMessage=(error.message)
          
      }
    });

    

  }

  getusers():Observable<any[]>{
    const ref = this.db.collection('users');
    return ref.valueChanges({idField: 'id'});
  }

  Logout(){
    this.afAuth.auth.signOut().then(user => {
      
     this.router.navigate (['/welcome'])
    })}
 

  login(email:string, password:string){
    this.afAuth.auth.signInWithEmailAndPassword(email,password).catch((error)=>alert(error.message)).then(user => {
      
      if (user){this.router.navigate (['/welcome'])}
      else {this.router.navigate (['/login'])
      // this.errorMessage=(error.message)
      
  }
    })
}



}
