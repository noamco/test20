import { Component, OnInit } from '@angular/core';
import { Posts } from './../interfaces/posts';
import { Observable } from 'rxjs';
import { PostsService } from './../posts.service';
import { ActivatedRoute } from '@angular/router';
import { Users } from '../interfaces/users';
import { post } from 'selenium-webdriver/http';
import { Comments } from '../interfaces/comments';
import { Title } from '@angular/platform-browser';
import { AuthService } from '../auth.service';


@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  // posts$:Observable<any[]>;

  // constructor(private postsservice: PostsService) { }

  // ngOnInit() {
  //   this.posts$ = this.postsservice.getPosts();
    
    
  // }

  // deletePost(id:string){
  //   this.postsservice.deletePost(id);

  // }

  posts$: Posts[];
  users$: Users[];
  comments$: Comments[];
  title:string;
  body:string;
  author:string;
  ans:string="";
  id:string;
  userId: string;
  post: any;
  num:number;

  constructor(private PostsService: PostsService,private authService:AuthService) { }

  // constructor(private PostsService: PostsService) { }
  // myFunc(){
  //   for (let index = 0; index < 2; index++) {
  //     for (let i = 0; i < this.users$.length; i++) {
  //       if (this.posts$[index].userId==this.users$[i].id) {
  //         this.title = this.posts$[index].title;
  //         this.body = this.posts$[index].body;
  //         this.PostsService.addPosts(this.body,this.title);   
  //       }  
  //     } 
  //   }
  // this.ans ="Saved for later viewing"
  // }

  MyFunc(id:number){
    this.num=id;
      for (let index = 0; index < this.posts$.length; index++) {
        if (this.posts$[index].id == this.num) {
            this.title = this.posts$[index].title;
            this.body = this.posts$[index].body;
            this.PostsService.addPost(this.userId,this.title,this.num,this.body,0);   
            
          
          }  
              
            }
            this.ans="Saved for later viewing";
          }
        


  // addPost(title:string, body:string){
  //   this.PostsService.addPost(this.title, this.body)
  //   this.ans ="Saved for later viewing"
  // }
  
  // getPost(){
  //   this.PostsService.getPost(this.id, this.userId).subscribe(
  //     book => {
  //       console.log(post.data().author)
  //       console.log(book.data().title)
  //       this.author = book.data().author;
  //       this.title = book.data().title;})        
  // }
 
  
  ngOnInit() {
    this.PostsService.getPosts()
      .subscribe(posts => this.posts$ = posts);
    this.PostsService.getUsers()
      .subscribe(users => this.users$ = users);
    this.PostsService.getComments()
    .subscribe(comments => this.comments$ = comments);

    this.authService.getUser().subscribe(
            user => {
              this.userId = user.uid; }
          )
        
  }

}