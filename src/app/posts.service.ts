import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { getMultipleValuesInSingleSelectionError } from '@angular/cdk/collections';
import { Posts } from './interfaces/posts';
import { Users } from './interfaces/users';
import { Comments } from './interfaces/comments';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AuthService } from './auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  apiUrl = 'https://jsonplaceholder.typicode.com/posts/';
  apiUrlUser = 'https://jsonplaceholder.typicode.com/users';
  apiUrlComment = 'https://jsonplaceholder.typicode.com/comments';

  userCollection: AngularFirestoreCollection = this.db.collection('users');


constructor(private _http: HttpClient, private db:AngularFirestore) {

//  constructor(private _http: HttpClient,private db:AngularFirestore, 
//               private PostService:PostsService,
//               private router:Router,
//               private route:ActivatedRoute,
//               private authservice:AuthService ) {
    
   }

   userId: string;
   title:string;
   body:string;
   id:string;
  savedpostsCollection:AngularFirestoreCollection;


   addPosts(body:String, name:String){
    const post = {body:body, name:name};
    this.userCollection.doc(this.userId).collection('posts').add(post);
  }
   getPosts() {
    return this._http.get<Posts[]>(this.apiUrl);}
   getUsers() {
      return this._http.get<Users[]>(this.apiUrlUser);  
    }
    getComments() {
      return this._http.get<Comments[]>(this.apiUrlComment);  
    }

    getPost(id:string, userId:string):Observable<any>{
      return this.db.doc(`users/${userId}/posts/${id}`).get()
    }

    addPost(userId:string, title:string, num:number, body:string, likes:number){
          const post = {title:title, num:num, body:body, likes:0}
          this.userCollection.doc(userId).collection('posts').add(post);
        }
  
        getlist(userId): Observable<any[]> {
    
          this.savedpostsCollection = this.db.collection(`users/${userId}/posts`);
          console.log('showing list of classified');
          return this.savedpostsCollection.snapshotChanges().pipe(
            map(collection => collection.map(document => {
              const data = document.payload.doc.data();
              data.id = document.payload.doc.id;
              return data;
            }))
          );    
        } 

        addLikes(id:string ,title:string , body:string ,author:string)
    {
      
    }

    // addPost(userId:string, title:string, body:string){
    //   this.PostService.addPost(this.userId, this.title, this.body);
    // }

    // savePost(userId:string, title:string, body:string){
    //   const post = {title:title, body:body}
    //   // this.db.collection('books').add(book);
    //   this.userCollection.doc(userId).collection('posts').add(post);
    // }

    // constructor(private http: HttpClient, private db: AngularFirestore,
    //   private authService:AuthService) { }

    //   getPosts():Observable<any[]>{
    //     const ref = this.db.collection('posts');
    //     return ref.valueChanges({idField: 'id'});
    //   }

    //   deletePost(id:string){
    //     this.db.doc(`posts/${id}`).delete();
    //   }
}
