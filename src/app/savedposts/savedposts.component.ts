import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-savedposts',
  templateUrl: './savedposts.component.html',
  styleUrls: ['./savedposts.component.css']
})
export class SavedpostsComponent implements OnInit {

  classifeds$:Observable<any[]>;
  users$:Observable<any[]>;
  userId:string;
  likes = 0;

  constructor(private ClassifyService:ClassifyService,
    public authService:AuthService) { }


  ngOnInit() {
    this.users$ = this.authService.getusers()
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.classifeds$ = this.ClassifyService.getlist(this.userId); 
      }
    )
  }

  delete(id:string){
    this.ClassifyService.deletePost(id, this.userId);
  }

  addLikes(){
    this.likes++
  }

  }



